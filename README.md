## End-to-end Test for proto.de Signup Process

> Test based on [Puppeteer](https://pptr.dev) tool

### Installation

```markdown
\$ npm install
```

### Start

Using platform indicators you can run test for specific viewports: tv, desktop, tablet and mobile

```markdown
\$ npm run e2e:desktop
```

### Documentation

[Puppeteer Docs](https://devdocs.io/puppeteer/index)
