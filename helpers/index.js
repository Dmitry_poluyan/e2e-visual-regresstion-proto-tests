import { APP_ENV, TP_HOSTS, proto_DE_HOSTS, proto_V3_HOSTS } from './commonConfig.js';

export function isDevEnv() {
    return process.env.APP_ENV === APP_ENV.DEVELOPMENT;
}

export function isStagingEnv() {
    return process.env.APP_ENV === APP_ENV.STAGING;
}

export function isProdEnv() {
    return process.env.APP_ENV === APP_ENV.PRODUCTION;
}

export function getprotoHost() {
    if (isDevEnv()) {
        return proto_DE_HOSTS.DEVELOPMENT;
    }
    if (isStagingEnv()) {
        return proto_DE_HOSTS.STAGING;
    }
    if (isProdEnv()) {
        return proto_DE_HOSTS.PRODUCTION;
    }

    return proto_DE_HOSTS.STAGING;
}

export function getTPHost() {
    if (isDevEnv()) {
        return TP_HOSTS.DEVELOPMENT;
    }
    if (isStagingEnv()) {
        return TP_HOSTS.STAGING;
    }
    if (isProdEnv()) {
        return TP_HOSTS.PRODUCTION;
    }

    return TP_HOSTS.STAGING;
}

export function getprotoV3Host() {
    if (isDevEnv()) {
        return proto_V3_HOSTS.DEVELOPMENT;
    }
    if (isStagingEnv()) {
        return proto_V3_HOSTS.STAGING;
    }
    if (isProdEnv()) {
        return proto_V3_HOSTS.PRODUCTION;
    }

    return proto_V3_HOSTS.STAGING;
}

export function delay() {
    return new Promise((resolve => {
        setTimeout(resolve, 5000);
    }));
}
