export const generateCounterNumber = () => `e2e${Date.now()}${Math.random().toString().slice(2)}`;

export const generateEmail = () => `e2e-test-${Date.now()}@proto-proto.de`;

export const generateUserName = () => `e2e-test-${Date.now()}-${Math.random().toString().slice(2)}`;

export const generateTerminationDate = () => `12.12.${new Date().getFullYear() + 1}`;
