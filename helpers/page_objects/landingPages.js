import { SELECTORS } from '../commonConfig.js';

export async function unlockProtectedPage(page, password) {
    const {
        landing: { authForm }
    } = SELECTORS;

    await page.waitForSelector(authForm.formClass);
    await page.type(authForm.inputSelector, password);
    await page.focus(authForm.submitButtonClass);
    await page.keyboard.type('\n');
    await page.waitForTimeout(200);
}
