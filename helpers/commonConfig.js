export const APP_ENV = { DEVELOPMENT: 'development', PRODUCTION: 'production', STAGING: 'staging' };

export const USER_AGENT = 'proto-puppeteer';

export const proto_DE_HOSTS = {
    PRODUCTION: 'https://proto.de',
    STAGING: 'https://staging.proto.de',
    DEVELOPMENT: 'http://localhost:3000'
};

export const TP_HOSTS = {
    PRODUCTION: 'https://proto.proto.de',
    STAGING: 'https://proto.staging.proto.de',
    DEVELOPMENT: 'http://localhost:3001'
};

// TODO: dev is used 3000 port the same like deSite
export const proto_V3_HOSTS = {
    PRODUCTION: 'https://v3.proto.de',
    STAGING: 'https://v3.dev.proto.de',
    DEVELOPMENT: 'http://localhost:3000'
};

export const TIMEOUT = 10 * 60 * 1000;

export const NETWORK_PRESETS = {
    GPRS: {
        offline: false,
        downloadThroughput: (50 * 1024) / 8,
        uploadThroughput: (20 * 1024) / 8,
        latency: 500
    },
    Regular2G: {
        offline: false,
        downloadThroughput: (250 * 1024) / 8,
        uploadThroughput: (50 * 1024) / 8,
        latency: 300
    },
    Good2G: {
        offline: false,
        downloadThroughput: (450 * 1024) / 8,
        uploadThroughput: (150 * 1024) / 8,
        latency: 150
    },
    Regular3G: {
        offline: false,
        downloadThroughput: (750 * 1024) / 8,
        uploadThroughput: (250 * 1024) / 8,
        latency: 100
    },
    Good3G: {
        offline: false,
        downloadThroughput: (1.5 * 1024 * 1024) / 8,
        uploadThroughput: (750 * 1024) / 8,
        latency: 40
    },
    Regular4G: {
        offline: false,
        downloadThroughput: (4 * 1024 * 1024) / 8,
        uploadThroughput: (3 * 1024 * 1024) / 8,
        latency: 20
    },
    DSL: {
        offline: false,
        downloadThroughput: (2 * 1024 * 1024) / 8,
        uploadThroughput: (1024 * 1024) / 8,
        latency: 5
    },
    WiFi: {
        offline: false,
        downloadThroughput: (30 * 1024 * 1024) / 8,
        uploadThroughput: (15 * 1024 * 1024) / 8,
        latency: 2
    }
};

// TODO: Enable tests for the viewports (@Pavel)
export const LANDSCAPE = {
    MOBILE_OPTIONS: {
        width: 320,
        height: 568,
        isMobile: true,
        hasTouch: true
    },
    DESKTOP_OPTIONS: {
        width: 1280,
        height: 762,
        isMobile: false,
        hasTouch: false
    },
    TV_OPTIONS: {
        width: 1920,
        height: 1080,
        isMobile: false,
        hasTouch: false
    },
    TABLET_OPTIONS: {
        width: 768,
        height: 1024,
        isMobile: true,
        hasTouch: true
    }
};

export const SELECTORS = {
    landing: {
        authForm: {
            formClass: '.proto-simple-form',
            inputSelector: 'input[name="password"]',
            submitButtonClass: '.proto-simple-form_submit-button'
        }
    },
    protos: {
        mapSwitcher: '.proto-de-protos-filter-section .proto-switcher-button',
        buttonToOpenTariffDetailsModal: '.proto-de-protos-section .proto-producer-data__details-button:first-child'
    },
    emailService: {
        emailInbox: '#depublicemailinbox li:first-child',
        iframe: 'iframe',
        popupUsername: '#popupusername',
        confirm: '.swal2-confirm',
        container: '.swal2-container'
    }
};
