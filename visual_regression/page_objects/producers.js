import { SELECTORS } from '../../helpers/commonConfig.js';

export async function openprotosMap(page /* , options = {} */) {
    await page.waitForSelector(SELECTORS.protos.mapSwitcher);
    await page.focus(SELECTORS.protos.mapSwitcher);
    await page.click(SELECTORS.protos.mapSwitcher);
}

export async function openTariffDetailsModal(page) {
    await page.waitForSelector(SELECTORS.protos.buttonToOpenTariffDetailsModal);
    await page.focus(SELECTORS.protos.buttonToOpenTariffDetailsModal);
    await page.evaluate(
        selector => document.querySelector(selector).click(),
        SELECTORS.protos.buttonToOpenTariffDetailsModal
    );
}
