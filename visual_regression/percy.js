/**
 * Run this test by command: "npm run test:visual:percy"
 * Generate screenshots in Percy for pages
 * Screenshots and difference saves in 'https://percy.io/DzmitryPol/proto'
 * Choose displays dimensions for generating in options for "percySnapshot"
 * Pages, for which necessary generate screens are configured in constants.js
 * After generating check pages for correct view!
 * Be care with quantity of pages and screenshot dimensions,
 * per each page/screenshot generates 2 images
 * Percy have limits for images in month
 */

import percySnapshot from '@percy/puppeteer';
import async from 'async';
import {
    scrollLazyImages,
    acceptCookies,
    getAction,
    getPageUrl,
    scrollToTop,
    stopVideo,
    stopPageSliders,
    getRemark
} from './testHelpers.js';
import { pages, goToConfig } from './constants.js';
import { launchBrowser, createNewPage } from '../e2e/page_objects/init.js';

const widths = [375, 1920];

const percyCustomCss = `
    video::-webkit-media-controls-enclosure {
        display: none;
    }
    
    .parallax-block-vertical, .parallax-block-horizontal {
        transform: none !important;
    }
    
    .proto-slider-wrapper {
        transform: translate3d(0px, 0px, 0px) !important;
    }
`;

(async () => {
    await async.eachLimit(pages, 10, async item => {
        const browser = await launchBrowser();
        const page = await createNewPage(browser);

        await page.setBypassCSP(true);
        await page.setDefauprotoavigationTimeout(0);

        const pageUrl = getPageUrl(item);
        const action = getAction(item);
        const pageRemark = getRemark(item);

        await page.goto(pageUrl, goToConfig);

        try {
            action && (await action(page)) && (await page.waitForTimeout(1000));
        } catch (error) {
            console.error('Action error:', error);
        } finally {
            await snapshotTests(page, pageUrl, pageRemark);
        }
        await page.close();
        await browser.close();
    });
})();

async function snapshotTests(page, url, remark) {
    for (const width of widths) {
        await page.setViewport({ width, height: 600 });
        await page.evaluate(acceptCookies);
        await page.evaluate(scrollLazyImages, 1);
        await page.evaluate(scrollToTop);
        await page.evaluate(stopPageSliders);
        await page.evaluate(stopVideo);
        await page.waitForTimeout(1000);
        await percySnapshot(page, `${url}-${width}${remark ? `-${remark}` : ''}`, {
            minHeight: 600,
            widths: [width],
            percyCSS: percyCustomCss
        });
    }
}
