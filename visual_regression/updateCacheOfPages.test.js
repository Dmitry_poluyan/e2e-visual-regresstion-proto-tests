import { TIMEOUT, USER_AGENT } from '../helpers/commonConfig';
import { getPageUrl } from './testHelpers';
import { pages } from './constants';
import config from '../e2e/e2e.config';
import { createNewPage, launchBrowser } from '../e2e/page_objects/init';

const { SCREENSHOTS_PATH } = config;

describe('Update cache of pages', () => {
    for (const pageData of pages) {
        const pageUrl = getPageUrl(pageData);

        it.concurrent(
            pageUrl,
            async () => {
                console.log(`Updating cache for "${pageUrl}".`);

                const browser = await launchBrowser();
                const page = await createNewPage(browser);
                try {
                    await page.setUserAgent(USER_AGENT);
                    await page.setBypassCSP(true);
                    await page.goto(pageUrl);

                    await page.close();
                    await browser.close();
                } catch (e) {
                    await page.screenshot({ path: SCREENSHOTS_PATH() });
                    await page.screenshot({ path: SCREENSHOTS_PATH('full-'), fullPage: true });
                    await page.close();
                    await browser.close();
                    throw e;
                }
            },
            TIMEOUT
        );
    }
});
