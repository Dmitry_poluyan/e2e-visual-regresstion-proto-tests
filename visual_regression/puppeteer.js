/**
 * Run this test by command: "npm run test:visual:puppeteer"
 * Generate screenshots for pages
 * Screenshots saves to directory './__tests__/visual_regression/screenshots'
 * Choose displays dimensions for generating in const "screens"
 * Pages, for which necessary generate screens are configured in constants.js
 * After generating check pages for correct view!
 */

import puppeteer from 'puppeteer';

import {
    createDirectory,
    reset100vhStyle,
    clearDirectory,
    stopPageSliders,
    stopCarousels,
    scrollLazyImages,
    acceptCookies,
    getPageUrl,
    stopParallax,
    scrollToTop,
    getAction
} from './testHelpers.js';

import { USER_AGENT, NETWORK_PRESETS } from '../helpers/commonConfig.js';

import { screensOptions, goToConfig, pages, launchOptions, newPageConfig } from './constants.js';

const screens = ['desktop', 'laptop', 'tablet', 'mobile'];

(async () => {
    console.log('Start screenshot test at: ', new Date());

    const browser = await puppeteer.launch(launchOptions);

    clearDirectory('./visual_regression/screenshots');
    createDirectory('./visual_regression/screenshots');
    screens.forEach(screen => createDirectory(`./visual_regression/screenshots/${screen}`));

    for (const pageData of pages) {
        // TODO: Reuse createNewPage from init.js https://proto.atlassian.net/browse/SW-1506 (R)
        const pageUrl = getPageUrl(pageData);
        const action = getAction(pageData);
        const page = await browser.newPage(newPageConfig);
        await page.setUserAgent(USER_AGENT);

        // Connect to Chrome DevTools
        const client = await page.target().createCDPSession();

        // Set throttling property
        await client.send('Network.emulateNetworkConditions', NETWORK_PRESETS.WiFi);

        await page.goto(pageUrl, goToConfig);
        await page.evaluate(reset100vhStyle);
        try {
            action && (await action(page)) && (await page.waitForTimeout(1000));
        } catch (error) {
            console.error('Action error:', error);
        } finally {
            await makeScreenshots(page, pageUrl);
        }
        await page.close();
    }
    await browser.close();

    async function makeScreenshots(page, url) {
        for (const screen of screens) {
            await page.setViewport(screensOptions[screen]);
            await page.evaluate(acceptCookies);
            await page.evaluate(scrollLazyImages, 1);
            await page.evaluate(scrollToTop);
            await page.evaluate(stopPageSliders);
            await page.evaluate(stopCarousels);
            await page.waitForTimeout(500);
            await page.evaluate(stopParallax);
            await page.screenshot({
                type: 'png',
                path: `./visual_regression/screenshots/${screen}/screen_${url.match(/[^/]*$/i)[0]}.png`,
                fullPage: true
            });
            console.log(`Made "${url.match(/[^/]*$/i)[0]}.png" screenshot for ${screen}.`);
        }
    }

    console.log(`End screenshot test of ${pages.length} pages at: `, new Date());
})();
