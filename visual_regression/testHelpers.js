import fs from 'fs';
import path from 'path';

// TODO remove after fixing bug with 100vh in puppeteer (R)
export const reset100vhStyle = () => {
    // For common cases
    const style = document.createElement('style');
    style.innerHTML = '.proto-de-partners-landing .proto-de-partners-landing-main-section:first-child {min-height: auto}';

    // TODO: We have removed terra aqua page's code, should we remove this one? (R)
    // For terra-aqua
    const elMinHeight100vh =
        document.querySelector('.proto-de-partners-landing style:first-child + .proto-de-partners-landing-main-section') ||
        document.querySelector('.proto-de-partners-landing style:first-child+.proto-de-promo') ||
        document.querySelector('.proto-de-terraria-header');
    if (elMinHeight100vh) {
        elMinHeight100vh.style.minHeight = 'auto';
    }

    // For good energy campaign
    const elHeight100vh = document.querySelector('.proto-de-good-energy-campaign-section');
    if (elHeight100vh) {
        elHeight100vh.style.height = 'auto';
    }
};

export const createDirectory = dir => {
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
};

export const clearDirectory = dir => {
    removeDir(dir);
};

export const stopPageSliders = () => {
    const sliderBullets = Array.from(document.querySelectorAll('.proto-slider-container .proto-slider-bullet:first-child'));
    sliderBullets.forEach(bullet => {
        bullet.click();
    });
};

export const stopCarousels = () => {
    const carouselWrappers = Array.from(document.querySelectorAll('.proto-carousel-wrapper'));
    carouselWrappers.forEach(carouselWrapper => {
        carouselWrapper.style.transform = 'none';
    });
};

export const scrollLazyImages = async maxTimingPerImage => {
    await new Promise(resolve => {
        const images = Array.from(document.querySelectorAll('img'));
        const unloadedImages = [];

        scroll();

        function scroll() {
            const lazyImage = findFirstLazyImage();

            if (!lazyImage) {
                return resolve();
            }
            scrollToImage(lazyImage);
            return void loadImage(lazyImage);
        }

        function findFirstLazyImage() {
            return images.find(
                image => image.src === '' && unloadedImages.every(unloadedImage => unloadedImage !== image)
            );
        }

        function scrollToImage(image) {
            const imageYCoord = image.getBoundingClientRect().top + window.pageYOffset;
            window.scrollTo(0, imageYCoord);
        }

        function loadImage(image) {
            Promise.race([
                new Promise(resolveOperation => {
                    image.onload = () => {
                        resolveOperation();
                    };
                }),
                new Promise(resolveOperation => {
                    const imageLoadTimeoutSec = maxTimingPerImage || 2;
                    setTimeout(() => {
                        resolveOperation(image);
                    }, imageLoadTimeoutSec * 1000);
                })
            ]).then(result => {
                result && unloadedImages.push(result);
                scroll();
            });
        }
    });
};

export const acceptCookies = () => {
    const cookiesAlerts = Array.from(document.querySelectorAll('.proto-cookies-disclaimer--agree-button'));
    cookiesAlerts.forEach(alert => {
        alert.click();
    });
};

export const getPageUrl = page => {
    const pageUrl = typeof page === 'string' ? page : page.url;
    return pageUrl;
};

export const getAction = page => {
    const action = page && page.options && page.options.action;
    return action;
};

export const getRemark = page => {
    const remark = page && page.testRemark;
    return remark;
};

export const stopParallax = () => {
    const parallaxContainers = Array.from(
        document.querySelectorAll('.parallax-block-vertical, .parallax-block-horizontal')
    );
    parallaxContainers.forEach(container => {
        container.style.transform = 'none';
    });
};

export const scrollToTop = () => {
    window.scrollTo(0, 0);
};

export const stopVideo = () => {
    const videos = Array.from(document.querySelectorAll('video'));
    videos.forEach(video => {
        video.autoplay = false;
        video.loop = false;
        video.playsinline = false;
        video.pause();
        video.currentTime = 1;
    });
};

function removeDir(dirPath) {
    if (!fs.existsSync(dirPath)) {
        return;
    }

    const list = fs.readdirSync(dirPath);
    for (let i = 0; i < list.length; i++) {
        const filename = path.join(dirPath, list[i]);
        const stat = fs.statSync(filename);

        if (filename == '.' || filename == '..') {
            // do nothing for current and parent dir
        } else if (stat.isDirectory()) {
            removeDir(filename);
        } else {
            fs.unlinkSync(filename);
        }
    }

    fs.rmdirSync(dirPath);
}
