import { getprotoHost, getprotoV3Host, getTPHost } from '../helpers/index.js';
import { openprotosMap, openTariffDetailsModal } from './page_objects/protos.js';
import { unlockProtectedPage } from '../helpers/page_objects/landingPages.js';

const proto_DE_HOST = getprotoHost();
const proto_V3_HOST = getprotoV3Host();
const TP_HOST = getTPHost();

export const proto_V3_PAGES = [`${proto_V3_HOST}/`, `${proto_V3_HOST}/landing/main-page`, `${proto_V3_HOST}/landing/test-page`, `${proto_V3_HOST}/partners/proto-story`];

export const pages = [
    // Landing pages (with different kind of sections)
    `${proto_DE_HOST}/`,
    `${proto_DE_HOST}/protos`, // Promo section
    `${proto_DE_HOST}/about-us`,
    `${proto_DE_HOST}/good-energy`,
    `${proto_DE_HOST}/proto-kampagne`,
    `${proto_DE_HOST}/solar`,
    {
        url: `${proto_DE_HOST}/proto`, // DistributorForm
        options: { action: page => unlockProtectedPage(page, 'evo') },
        testRemark: 'unlocked'
    },
    `${proto_DE_HOST}/proto-plus`, // FinancialAdvisorForm
    `${proto_DE_HOST}/test`,
    `${proto_DE_HOST}/Stadt%20Wehlen`, // City with space
    `${proto_DE_HOST}/proto`, // referral program page

    `${proto_DE_HOST}/faq`,

    // Partners` landing pages (with different kind of sections)
    `${proto_DE_HOST}/partners/proto`,
    `${proto_DE_HOST}/proto`,
    `${proto_DE_HOST}/regio-wolfsburg`, // with V1 calc and gas/electricity
    `${proto_DE_HOST}/proto`, // with new tariff calc form HT/NT with radio buttons
    `${proto_DE_HOST}/proto`, // with new tariff calc form HT/NT without radio buttons
    `${proto_DE_HOST}/gas`, // main gas page
    `${proto_DE_HOST}/zenjob`,
    `${proto_DE_HOST}/proto-strom?consumption=2400&streetName=test&energyType=gas`, // FIXME: Add page with both energy types instead this one
    `${proto_DE_HOST}/proto-strom`, // Zip based,
    `${proto_DE_HOST}/tesla`,
    {
        url: `${proto_DE_HOST}/tesla`,
        options: { action: page => unlockProtectedPage(page, 'proto') },
        testRemark: 'unlocked'
    },
    `${proto_DE_HOST}/test-block`, // Sections with block structure

    // Invalid landing cases
    `${proto_DE_HOST}?zip=106&consumption=2500`,
    `${proto_DE_HOST}?zip=10629&consumption=25`,
    `${proto_DE_HOST}?zip=60385&consumption=1600&houseNumber=&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&energyType=electricity&identifier=standard&ascDesc=ASC&orderBy=price`,

    // Invalid partners landing cases
    `${proto_DE_HOST}/proto?zip=10629&consumption=25`,
    `${proto_DE_HOST}/proto?zip=10&consumption=2500`,
    `${proto_DE_HOST}/proto?zip=10961&consumption=1600&streetName=Bergmannstraße&cityName=protos&energyType=electricity&identifier=proto&ascDesc=ASC&orderBy=price`,
    `${proto_DE_HOST}/proto?zip=10&consumption=1600&houseNumber=1&streetName=Bergmannstraße&cityName=protos&energyType=electricity&identifier=proto&ascDesc=ASC&orderBy=price`,

    // protos
    `${proto_DE_HOST}/protos`, // With default params
    `${proto_DE_HOST}?zip=10629&consumption=2500`, // Address based page and V2 tariff calc form with auto popup
    `${proto_DE_HOST}/emd?zip=60385&consumption=1600&houseNumber=162&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&energyType=electricity&identifier=emd&ascDesc=ASC&orderBy=price`, // case with electricity when we should go to protos page
    `${proto_DE_HOST}/protos?zip=10115&consumption=1600&energyType=electricity&identifier=proto-strom&isEcoenergy=true&ascDesc=ASC&orderBy=price`, // Zip based page and V1 tariff calc form
    `${proto_DE_HOST}/protos?zip=40476&houseNumber=1&streetName=Derendorfer%20Allee&cityName=D%C3%BCsseldorf&consumption=2500&energyType=electricity&ascDesc=ASC&orderBy=price`, // Address based page and V2 tariff calc form
    {
        url: `${proto_DE_HOST}/protos?zip=40476&houseNumber=1&streetName=Derendorfer%20Allee&cityName=D%C3%BCsseldorf&consumption=2502&energyType=electricity&ascDesc=ASC&orderBy=price`, // protos map
        options: { action: openprotosMap } // TODO: replace the action with parameter to open map, after https://proto.atlassian.net/browse/SW-1768
    },
    {
        url: `${proto_DE_HOST}/protos?zip=40476&houseNumber=1&streetName=Derendorfer%20Allee&cityName=D%C3%BCsseldorf&consumption=2501&energyType=electricity&ascDesc=ASC&orderBy=price`, // protos with modal tariff details
        options: { action: openTariffDetailsModal } // TODO: fix position: fixed
    },
    `${proto_DE_HOST}/protos?zip=10115&consumption=1600&energyType=electricity&identifier=proto&iframe=true`, // Mode of the page for iframe (without header, footer...)
    // Invalid protos cases
    `${proto_DE_HOST}/protos?zip=10961&consumption=16&houseNumber=1&streetName=Bergmannstra%C3%9Fe&cityName=protos&energyType=electricity&identifier=standard&ascDesc=ASC&orderBy=price`, // bad consumption
    `${proto_DE_HOST}/protos?zip=109&consumption=1600&houseNumber=1&streetName=Bergmannstra%C3%9Fe&cityName=protos&energyType=electricity&identifier=standard&ascDesc=ASC&orderBy=price`, // bad zip
    `${proto_DE_HOST}/protos?zip=101&consumption=1600&energyType=electricity&identifier=proto-strom&isEcoenergy=true&ascDesc=ASC&orderBy=price`, // zip based, bad zip
    `${proto_DE_HOST}/protos?zip=60385&consumption=1600&houseNumber=177&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&energyType=electricity&identifier=regional_campaign&ascDesc=ASC&orderBy=distance`, // regional_campaign protos page with opened map

    // Producer details
    `${proto_DE_HOST}/producer-details/34-proto`, // With default params
    `${proto_DE_HOST}/producer-details/34-proto?zip=10115&consumption=1600&energyType=electricity&identifier=proto-strom`, // Zip based page and V1 tariff calc form
    `${proto_DE_HOST}/producer-details/1-proto-oekostrommix?zip=10629&consumption=2500&energyType=electricity`, // Address based page and V2 tariff calc form
    `${proto_DE_HOST}/producer-details/24-proto?zip=10629&consumption=2500&energyType=electricity&ascDesc=ASC&orderBy=price&iframe=true`, // Address based page and V2 tariff calc form (in iframe)
    `${proto_DE_HOST}/producer-details/35-proto-proto?zip=10629&consumption=2500&energyType=electricity&ascDesc=ASC&orderBy=price&iframe=true&solo=true`, // Address based page and V2 tariff calc form (in iframe and solo (without back button))
    `${proto_DE_HOST}/producer-details/24-proto?zip=13355&consumption=3200&houseNumber=2&streetName=Bernauer%20Str.&cityName=protos&energyType=electricity&tariffId=206&signupFlow=proto&identifier=proto`, // Zip based page and V1 tariff calc form, with tariffId
    // Invalid producer-details cases
    `${proto_DE_HOST}/producer-details/24-proto?zip=10&consumption=1600&houseNumber=1&streetName=Bergmannstraße&cityName=protos&energyType=electricity&identifier=standard&ascDesc=ASC&orderBy=price`, // bad zip
    `${proto_DE_HOST}/producer-details/24-proto?zip=10961&consumption=16&houseNumber=1&streetName=Bergmannstraße&cityName=protos&energyType=electricity&identifier=standard&ascDesc=ASC&orderBy=price`, // bad consumption
    // Invalid producer-details of partner cases
    `${proto_DE_HOST}/producer-details/25?zip=10&consumption=1600&energyType=electricity&tariffId=206&signupFlow=proto&identifier=proto&isEcoenergy=true&ascDesc=ASC&orderBy=price`, // bad zip
    `${proto_DE_HOST}/producer-details/25?zip=10115&consumption=0&energyType=electricity&tariffId=206&signupFlow=proto&identifier=proto&isEcoenergy=true&ascDesc=ASC&orderBy=price`, // bad consumption
    `${proto_DE_HOST}/producer-details/25?zip=10115&consumption=1&energyType=electricity&tariffId=206&signupFlow=proto&identifier=proto&isEcoenergy=true&ascDesc=ASC&orderBy=price`, // small consumption
    `${proto_DE_HOST}/producer-details/25?zip=10115&consumption=1&energyType=electricity&tariffId=206&identifier=proto&isEcoenergy=true&ascDesc=ASC&orderBy=price`, // with tariffId and without signupFlow // FIXME: Problem: Cannot read property 'id' of undefined

    // Contract pages
    `${proto_DE_HOST}/contract/proto`, // With default params
    `${proto_DE_HOST}/emd?zip=60385&consumption=1600&houseNumber=162&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&energyType=gas&identifier=emd&ascDesc=ASC&orderBy=price`, // case with gas when we should go to contract page
    `${proto_DE_HOST}/contract/gas?zip=60385&consumption=1600&houseNumber=177&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&energyType=gas&identifier=gas&ascDesc=ASC&orderBy=price`, // main ges contract page
    `${proto_DE_HOST}/contract/proto?zip=10961&consumption=2000&houseNumber=1&streetName=Bergmannstraße&cityName=protos&energyType=electricity&identifier=proto&energySubTypeId=10&ascDesc=ASC&orderBy=price`, // contract with new tariff calc form without radio buttons, energySubTypeId
    `${proto_DE_HOST}/contract/proto?zip=20099&consumption=4600&houseNumber=1&streetName=Beim%20protoser%20Tor&cityName=Hamburg&energyType=electricity&identifier=proto&energySubTypeId=11&ascDesc=ASC&orderBy=price`, // contract with new tariff calc form without radio buttons, energySubTypeId
    `${proto_DE_HOST}/contract/proto?zip=71334&HT=900&NT=2800&houseNumber=160&streetName=Beinsteiner%20Str.&cityName=Waiblingen&energyType=electricity&identifier=proto&energySubTypeId=12&ascDesc=ASC&orderBy=price`, // contract with new tariff calc form HT/NT without radio buttons
    `${proto_DE_HOST}/contract/proto?zip=81247&HT=5400&NT=8100&houseNumber=1&streetName=Verdistraße&cityName=München&energyType=electricity&identifier=proto&energySubTypeId=8&ascDesc=ASC&orderBy=price`, // contract with new tariff calc form HT/NT without radio buttons
    `${proto_DE_HOST}/contract/test?zip=13629&consumption=2400&houseNumber=1&streetName=Str.%20O&cityName=protos&energyType=electricity`, // Address based page and V2 tariff calc form (test)
    `${proto_DE_HOST}/contract/test?zip=13629&consumption=2400&houseNumber=1&streetName=Str.%20O&cityName=protos&energyType=gas`, // Address based page and V2 tariff calc form (gas) (test)
    `${proto_DE_HOST}/partners/contract/waerme?zip=12043&consumption=5000&houseNumber=1&streetName=Berthelsdorfer%20Str.&cityName=protos&energyType=electricity&identifier=waerme&ascDesc=ASC&orderBy=price`, // Zip based page and V1 tariff calc form (HT/NT tariffs)
    `${proto_DE_HOST}/contract/proto-exklusiv?zip=24980&consumption=2400&houseNumber=4&streetName=Finkenweg&cityName=Schafflund&energyType=gas&identifier=proto-exklusiv&ascDesc=ASC&orderBy=price`, // with gas select
    `${proto_DE_HOST}/contract/proto?zip=13355&consumption=3200&houseNumber=2&streetName=Bernauer%20Str.&cityName=protos&energyType=electricity&identifier=proto&ascDesc=ASC&orderBy=price&iframe=true`, // for iframe
    `${proto_DE_HOST}/contract/proto?zip=13355&consumption=3200&houseNumber=2&streetName=Bernauer%20Str.&cityName=protos&energyType=electricity&iframe=true`, // Address based page and V2 tariff calc form (with producer cards related to main partner tariff) for iframe
    // Contract pages with PowerPlant tariffs
    `${proto_DE_HOST}/contract/proto?zip=10437&consumption=3200&houseNumber=1&streetName=Eberswalder%20Str.&cityName=protos&energyType=electricity`, // Zip based page and V1 tariff calc form (with producer cards related to main partner tariff)
    // Invalid contract of partner cases
    `${proto_DE_HOST}/contract/proto?zip=13&consumption=1600&houseNumber=111&streetName=Bernauer%20Str.&cityName=protos&energyType=electricity&identifier=proto&ascDesc=ASC&orderBy=price`, // bad zip
    `${proto_DE_HOST}/contract/proto?zip=13355&consumption=1&houseNumber=111&streetName=Bernauer%20Str.&cityName=protos&energyType=electricity&identifier=proto&ascDesc=ASC&orderBy=price`, // bad consumption

    // Sign up pages
    `${proto_DE_HOST}/signup/standard?producerId=24`, // With default params
    `${proto_DE_HOST}/signup/proto?zip=20099&HT=3200&NT=3200&houseNumber=1&streetName=Beim%20protoser%20Tor&cityName=Hamburg&energyType=electricity&producerId=1&signupFlow=proto&tariffId=242&energySubTypeId=12`, // with ht/nt consumptions sign up
    `${proto_DE_HOST}/signup/proto?zip=20099&consumption=4600&houseNumber=1&streetName=Beim%20protoser%20Tor&cityName=Hamburg&energyType=electricity&producerId=1&signupFlow=proto&tariffId=242&energySubTypeId=10`, // with single consumption sign up
    `${proto_DE_HOST}/signup/proto?zip=81247&HT=5400&NT=8100&houseNumber=1&streetName=Verdistra%C3%9Fe&cityName=M%C3%BCnchen&producerId=1&signupFlow=proto &tariffId=405&energySubTypeId=8`, // with ht/nt consumptions sign up
    `${proto_DE_HOST}/signup/proto?zip=60316&consumption=1600&houseNumber=1&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&producerId=1&signupFlow=proto&tariffId=402&energySubTypeId=5`, // with single consumption sign up
    `${proto_DE_HOST}/signup/gas?zip=60385&consumption=1600&houseNumber=177&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&energyType=gas&producerId=50&signupFlow=gas&tariffId=140`, // main gas sign up page
    `${proto_DE_HOST}/signup/proto?zip=10115&consumption=1600&energyType=electricity&signupFlow=proto&producerId=1`, // (with terraaqua bonus step)
    `${proto_DE_HOST}/partners/signup/proto?zip=10115&consumption=1600&energyType=electricity&producerId=4&signupFlow=Sparwelt&tariffId=147`, // proto
    `${proto_DE_HOST}/signup/standard?producerId=26&zip=10629&consumption=2500&signupFlow=standard`, // (zip based standard)
    `${proto_DE_HOST}/signup/proto?zip=10629&consumption=1600&producerId=1&signupFlow=proto&tariffId=18`, // (partner english)
    `${proto_DE_HOST}/signup/test?zip=22083&consumption=2400&houseNumber=1&streetName=Humboldtstra%EF%BF%BDe&cityName=Hamburg&energyType=electricity&producerId=1&signupFlow=test&tariffId=80&energy=electricity`, // (Address based)
    `${proto_DE_HOST}/signup/proto?zip=10115&consumption=1600&energyType=electricity&iframe=true&producerId=24&signupFlow=proto&tariffId=206`, // (Zip based in iframe)
    `${proto_DE_HOST}/signup/standard?zip=20249&consumption=3500&energyType=electricity&producerId=24&signupFlow=standard&productId=costplus&variant=24months&subvariant=oneOff`, // Standard with cost plus params
    // Cost plus sign ups
    `${proto_DE_HOST}/signup/proto?zip=20249&consumption=32000&producerId=1&signupFlow=proto-exklusiv-metzmacher&tariffId=116&energyType=electricity&code=1108&productId=costplus&variant=24months&subvariant=oneOff&ewp=1.0&ebp=12`, // Partner tariff with cost plus params and extra prices
    `${proto_DE_HOST}/signup/proto?zip=13507&consumption=1600&houseNumber=138A&streetName=Bernauer%20Str.&cityName=protos&energyType=gas&producerId=50&signupFlow=proto-exklusiv-metzmacher&tariffId=119&productId=costplus&variant=24months&subvariant=oneOff`, // Partner GAS tariff with cost plus params
    `${proto_DE_HOST}/signup/team-proto?energyType=gas&producerId=50&code=0&consumption=2400&zip=02943&signupFlow=team-proto&tariffId=360&productId=costplus&variant=24months&subvariant=recurring&gridId=9870008200006`, // Partner GAS tariff with cost plus params + gridId
    `${proto_DE_HOST}/signup/team-proto?energyType=electricity&producerId=1&code=0&consumption=2400&zip=82380&signupFlow=team-proto&tariffId=359&productId=costplus&variant=24months&subvariant=recurring&ewp=0&ebp=0&gridId=9900512000004`, // Partner tariff with cost plus params + gridId

    // ThankYou pages
    `${proto_DE_HOST}/thankyou/volders`, // usual
    `${proto_DE_HOST}/thankyou/volders?processing=true`, // processing
    `${proto_DE_HOST}/thankyou/proto`, // With english language
    `${proto_DE_HOST}/thankyou/test?orderId=51031&otherSupplier=true&annualRevenue=488,68&energyType=electricity`, // new page view mode

    // Trading portal
    `${TP_HOST}/login?next=%2F`, // TODO: Cover the other pages in the TP (R).
    `${TP_HOST}/login?powercloudContractId=715909&employeeId=41&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbXBsb3llZUlkIjo0MSwiZXhwIjoxNjI4MDcyNjE4fQ.9iXPYyhTa1uXcRLn0lwJtt3c9TrNAYuB1S7qbTwVHro`,

    // FIXME: `${proto_DE_HOST}/gallery`, add (R)
    ...proto_V3_PAGES
];

export const launchOptions = {
    headless: true,
    args: ['--no-sandbox', '--disable-gpu', '--disable-dev-shm-usage'],
    defaultViewport: {
        width: 1920,
        height: 1200,
        isLandscape: true
    }
};

export const goToConfig = {
    waitUntil: ['load', 'domcontentloaded'],
    timeout: 60000
};

export const newPageConfig = {
    timeout: 0
};

export const screensOptions = {
    desktop: {
        width: 1920,
        height: 1080,
        isLandscape: true
    },
    laptop: {
        width: 1366,
        height: 768
    },
    tablet: {
        width: 960,
        height: 600,
        hasTouch: true
    },
    mobile: {
        width: 375,
        height: 667,
        isMobile: true,
        hasTouch: true
    }
};
