export default {
    preset: 'jest-puppeteer',
    globals: {
        URL: 'https://staging.proto.de'
    },
    collectCoverageFrom: ['<rootDir>/e2e'],
    testMatch: ['**/*.test.js'],
    testPathIgnorePatterns: ['/node_modules/'],
    testURL: 'http://localhost:8080',
    verbose: true,
    maxWorkers: 4,
    maxConcurrency: 10
};
