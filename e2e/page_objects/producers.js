import config from '../e2e.config';

const { SELECTORS } = config;

export async function choosePowerPlant(page /* , options = {} */) {
    await page.waitForSelector(SELECTORS.protos.producerList);
    await page.waitForSelector(SELECTORS.protos.producerChooseButton);
    await page.focus(SELECTORS.protos.producerChooseButton);

    const signUpButton = await page.$(SELECTORS.protos.producerChooseButton);
    await signUpButton.evaluate(button => button.click());
}

export async function goToProducerDetails(page /* , options = {} */) {
    await page.waitForSelector(SELECTORS.protos.producerList);
    await page.waitForSelector(SELECTORS.protos.producerDetailsButton);
    await page.focus(SELECTORS.protos.producerDetailsButton);

    const producerDetailsButton = await page.$(SELECTORS.protos.producerDetailsButton);
    await producerDetailsButton.evaluate(button => button.click());
}

export async function clickBecomeCustomerButton(page /* , options = {} */) {
    await page.waitForSelector(SELECTORS.producerDetails.becomeCustomerButton);
    await page.focus(SELECTORS.producerDetails.becomeCustomerButton);

    const signUpButton = await page.$(SELECTORS.producerDetails.becomeCustomerButton);
    await signUpButton.evaluate(button => button.click());
}

export async function checkPowerPlantCards(page /* , options */) {
    // TODO: Check count of protos cards
    // TODO: Check producer cards prices

    await page.waitForSelector(SELECTORS.protos.producerList);
    await page.waitForSelector(SELECTORS.protos.producerCard);
    await page.focus(SELECTORS.protos.producerCard);

    const cards = await page.$$(SELECTORS.protos.producerCard);

    expect(cards.length > 0).toBe(true);
}

// TODO: Check protos filter (@Pavel)
// TODO: Check producer details page (@Pavel)
