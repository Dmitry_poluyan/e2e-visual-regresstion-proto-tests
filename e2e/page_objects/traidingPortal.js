import config, { TP_HOST } from '../e2e.config';

const { SELECTORS, VALUES, GO_TO_CONFIG } = config;

// Login page
export async function loginToTradingPortal(page, options = {}) {
    const { testValues, contactStep: { withExistedEmail = false } = {} } = options;

    await page.goto(`${TP_HOST}/login`, GO_TO_CONFIG);

    await page.waitForSelector(SELECTORS.tradingPortal.loginPage.container);

    await page.focus(SELECTORS.tradingPortal.loginPage.usernameInput);
    await page.type(
        SELECTORS.tradingPortal.loginPage.usernameInput,
        withExistedEmail ? VALUES.existedEmail : testValues.email
    );

    await page.focus(SELECTORS.tradingPortal.loginPage.passwordInput);
    await page.type(
        SELECTORS.tradingPortal.loginPage.passwordInput,
        withExistedEmail ? VALUES.existedPassword : VALUES.password
    );

    await page.waitForSelector(SELECTORS.tradingPortal.loginPage.submitButton);
    await page.focus(SELECTORS.tradingPortal.loginPage.submitButton);
    await page.keyboard.type('\n');
}
