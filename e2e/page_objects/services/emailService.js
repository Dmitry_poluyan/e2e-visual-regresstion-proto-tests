import { generateUserName } from '../../../helpers/generators';
import { SELECTORS } from '../../../helpers/commonConfig';

let page;

export async function generateEmailAddress(browser) {
    const username = generateUserName();
    if (!page) {
        page = await browser.newPage();
        await page.goto('https://console.mail7.io/admin/inbox/inbox');
    }
    await page.bringToFront();
    await page.deleteCookie({ name: 'deusername' });
    await page.reload();
    await page.waitForSelector(SELECTORS.emailService.popupUsername);
    await page.type(SELECTORS.emailService.popupUsername, username);
    await page.click(SELECTORS.emailService.confirm);
    await page.waitForSelector(SELECTORS.emailService.container, { hidden: true });
    return `${username}@mail7.io`;
}

export async function getLastMessage() {
    await page.bringToFront();
    while (true) {
        try {
            await page.waitForSelector(SELECTORS.emailService.emailInbox, { timeout: 10000 });
            break;
        } catch (e) {
            await page.reload();
        }
    }
    await page.click(SELECTORS.emailService.emailInbox);
    await page.waitForSelector(SELECTORS.emailService.iframe);
    await page.waitForTimeout(1000);
    return page.$eval(SELECTORS.emailService.iframe, e => e.contentWindow.document.body.innerHTML);
}
