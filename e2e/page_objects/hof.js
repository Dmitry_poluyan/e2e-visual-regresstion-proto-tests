import { logTestEvent } from '../services/protoAdmin';
import config from '../e2e.config';
import { isStagingEnv, isProdEnv } from '../../helpers';
import { launchBrowser, createNewPage } from './init';

const {
    LAUNCH_OPTIONS: { timeout },
    SCREENSHOTS_PATH
} = config;

export function e2eIt(blockName, options = {}, callback) {
    const shouldLogTestEvent = isStagingEnv() || isProdEnv();

    it.concurrent(
        blockName,
        async () => {
            // TODO: Move to the HOF all common functional (R, Dmitry)
            if (options.skipThisTestCase) {
                return;
            }

            const browser = await launchBrowser();
            const page = await createNewPage(browser);

            const e2eTestCaseName = expect.getState().currentTestName;

            if (shouldLogTestEvent) {
                logTestEvent(e2eTestCaseName, config.E2E_STATUSES.START);
            }

            // FIXME: Probably we can add try/catch here as wrapper of the callback and pass the error data to proto admin as post request and throw the error up. If need. This can be useful as history with unstable test cases (R, Dmitry)
            try {
                await callback(browser, page, () => {
                    if (shouldLogTestEvent) {
                        logTestEvent(e2eTestCaseName, config.E2E_STATUSES.SUCCESS);
                    }
                });

                await page.close();
                await browser.close();
            } catch (e) {
                await page.screenshot({ path: SCREENSHOTS_PATH() });
                await page.screenshot({ path: SCREENSHOTS_PATH('full-'), fullPage: true });
                await page.close();
                await browser.close();
                throw e;
            }
        },
        timeout
    );
}
