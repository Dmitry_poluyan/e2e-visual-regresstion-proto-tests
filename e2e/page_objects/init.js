import puppeteer from 'puppeteer';
import config from '../e2e.config.js';
import { LANDSCAPE } from '../../helpers/commonConfig.js';
// TODO: Use DeviceDescriptors (@Pavel)
// const devices = require('puppeteer/DeviceDescriptors');

// TODO: Use captcha (@Dmitry)
// import puppeteer from 'puppeteer-extra';
// const RecaptchaPlugin = require('puppeteer-extra-plugin-recaptcha');
//
// const recaptchaPlugin = RecaptchaPlugin({
//     provider: { id: '2captcha', token: '9666b83a6253a2235dc64c042092f123' }
// });
// puppeteer.use(recaptchaPlugin);

const {
    LAUNCH_OPTIONS,
    LAUNCH_OPTIONS: { timeout },
    NEW_PAGE_CONFIG,
    USER_AGENT
} = config;

const { VIEWPORT } = process.env;
// TODO: Enable tests for the viewports (@Pavel)
// FIXME: Use viewportOptions[VIEWPORT] || desktopOptions (@Pavel)
// TODO: Add to npm run e2e the other VIEWPORTs  (@Pavel)
const deviceOptions =
    VIEWPORT === 'mobile'
        ? LANDSCAPE.MOBILE_OPTIONS
        : VIEWPORT === 'tablet'
        ? LANDSCAPE.TABLET_OPTIONS
        : VIEWPORT === 'tv'
        ? LANDSCAPE.TV_OPTIONS
        : LANDSCAPE.DESKTOP_OPTIONS;

export async function launchBrowser(/* , options */) {
    return await puppeteer.launch({
        ...LAUNCH_OPTIONS,
        args: [...LAUNCH_OPTIONS.args, `--window-size=${deviceOptions.width},${deviceOptions.height}`]
    });
}

export async function createNewPage(browser /* , options */) {
    const page = await browser.newPage(NEW_PAGE_CONFIG);
    await page.setUserAgent(USER_AGENT);
    await page.setDefauprotoavigationTimeout(timeout);
    await page.setDefaultTimeout(timeout);

    return page;
}
