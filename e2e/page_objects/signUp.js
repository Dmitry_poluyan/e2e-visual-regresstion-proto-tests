import faker from 'faker';
import config from '../e2e.config';
import { generateEmail, generateTerminationDate } from '../../helpers/generators';

const { SELECTORS, VALUES, HTTP_METHODS, GOOGLE_RECAPTCHA } = config;

// IdentityStep
export async function fillIdentityStep(page, options = {}) {
    const {
        identityStep = {
            business: false,
            withDefaultBirthday: false,
            useRandomFirstName: false,
            useRandomLastName: false,
            salutation: 0
        }
    } = options;

    await page.waitForSelector(SELECTORS.signup.identity.container);

    if (identityStep.business) {
        await page.focus(SELECTORS.signup.identity.business);
        await page.click(SELECTORS.signup.identity.business);

        await page.focus(SELECTORS.signup.identity.company);
        await page.type(SELECTORS.signup.identity.company, VALUES.company);

        await page.focus(SELECTORS.signup.identity.legalForm);
        await page.type(SELECTORS.signup.identity.legalForm, VALUES.legalForm);
    }

    await selectSalutation(page, options);

    if (identityStep.useRandomFirstName) {
        await page.focus(SELECTORS.signup.identity.firstName);
        await page.type(SELECTORS.signup.identity.firstName, faker.name.firstName());
    } else {
        await page.focus(SELECTORS.signup.identity.firstName);
        await page.type(SELECTORS.signup.identity.firstName, VALUES.firstName);
    }

    if (identityStep.useRandomLastName) {
        await page.focus(SELECTORS.signup.identity.lastName);
        await page.type(SELECTORS.signup.identity.lastName, faker.name.lastName());
    } else {
        await page.focus(SELECTORS.signup.identity.lastName);
        await page.type(SELECTORS.signup.identity.lastName, VALUES.lastName);
    }

    if (!identityStep.withDefaultBirthday) {
        await page.focus(SELECTORS.signup.identity.birthday);
        await page.type(SELECTORS.signup.identity.birthday, VALUES.birthday);
    }
}

export async function selectSalutation(page, options) {
    const { identityStep = {} } = options;
    const { salutation = 0 } = identityStep;

    await page.focus(SELECTORS.signup.identity.salutation);
    await page.click(SELECTORS.signup.identity.salutation);

    await page.waitForSelector(SELECTORS.signup.identity.salutationList);
    await page.evaluate(selector => {
        document.querySelector(selector).scrollIntoView();
    }, SELECTORS.signup.identity.salutationList);
    await page.focus(SELECTORS.signup.identity.salutationList);
    await page.click(SELECTORS.signup.identity.salutationOption[salutation]);
}

// ContactStep
export async function fillContactStep(page, options = {}) {
    const { contactStep = { withNewPassword: false, withExistedEmail: false }, testValues } = options;
    await page.waitForSelector(SELECTORS.signup.contact.container);

    const emailAddress = testValues.email || generateEmail();

    if (contactStep.withExistedEmail) {
        await page.focus(SELECTORS.signup.contact.email);
        await page.type(SELECTORS.signup.contact.email, VALUES.existedEmail);

        await page.waitForTimeout(1000);

        await page.focus(SELECTORS.signup.contact.oldAccountCheckbox);
        await page.click(SELECTORS.signup.contact.oldAccountCheckbox);

        await page.focus(SELECTORS.signup.contact.password);
        await page.type(SELECTORS.signup.contact.password, VALUES.existedPassword);
        return;
    } else {
        await page.focus(SELECTORS.signup.contact.email);
        await page.type(SELECTORS.signup.contact.email, emailAddress);

        // TODO: Add case when we set, then remove password and sign up with default generated password
        if (contactStep.withNewPassword) {
            await page.focus(SELECTORS.signup.contact.password);
            await page.type(SELECTORS.signup.contact.password, VALUES.password);
            await page.focus(SELECTORS.signup.contact.passwordConfirmation);
            await page.type(SELECTORS.signup.contact.passwordConfirmation, VALUES.password);
            return;
        }
    }

    await page.focus(SELECTORS.signup.contact.phoneAreaCode);
    await page.type(SELECTORS.signup.contact.phoneAreaCode, VALUES.phoneAreaCode);

    await page.focus(SELECTORS.signup.contact.phone);
    await page.type(SELECTORS.signup.contact.phone, VALUES.phone);
}

// AddressStep
export async function fillAddressStep(page, options = {}) {
    const {
        addressStep = { billingAlternativeAddress: false, useDefaultAddressFromURL: false },
        contactStep = { withExistedEmail: false }
    } = options;
    await page.waitForSelector(SELECTORS.signup.address.container);

    if (contactStep.withExistedEmail) {
        return;
    }

    if (addressStep.useDefaultAddressFromURL) {
        // do nothing
    } else {
        // TODO: Add case with filling other address. Clean the fields when this is not empty
        await page.focus(SELECTORS.signup.address.street);
        await page.type(SELECTORS.signup.address.street, VALUES.street);
        await page.focus(SELECTORS.signup.address.streetNumber);
        await page.type(SELECTORS.signup.address.streetNumber, `${VALUES.streetNumber}-${Date.now()}`);

        await page.focus(SELECTORS.signup.address.zip);
        await page.waitForTimeout(1500);
    }

    if (addressStep.billingAlternativeAddress) {
        // TODO: Add case with billingAlternativeAddress: true
    }
}

// ConsumptionStep
export async function fillConsumptionStep(page, options) {
    const { testValues } = options;
    await page.waitForSelector(SELECTORS.signup.consumption.container);

    // TODO: Add case for changing "usage" field
    // TODO: Add cases for customerSpecification: "relocation_at" value and relocationDate
    // TODO: Add cases for hasPreviousContractTerminated: "false" and shouldCancelPreviousContract
    // TODO: Add case for typeOfMeterId = marketLocationIdentifier

    const terminationDate = generateTerminationDate();
    const counterNumber = testValues.counterNumber || `e2e${Date.now()}`;

    await page.waitForSelector(SELECTORS.signup.consumption.counterNumberCheckbox);
    await page.focus(SELECTORS.signup.consumption.counterNumberCheckbox);
    await page.click(SELECTORS.signup.consumption.counterNumberCheckbox);

    await page.waitForSelector(SELECTORS.signup.consumption.counterNumber);
    await page.focus(SELECTORS.signup.consumption.counterNumber);
    await page.type(SELECTORS.signup.consumption.counterNumber, counterNumber);

    await selectOptionOfPreviousProduct(page);

    await page.focus(SELECTORS.signup.consumption.terminationDate);
    await page.type(SELECTORS.signup.consumption.terminationDate, terminationDate);
}

export async function selectOptionOfPreviousProduct(page /* , options */) {
    await page.focus(SELECTORS.signup.consumption.previousProduct);
    await page.click(SELECTORS.signup.consumption.previousProduct);

    await page.waitForSelector(SELECTORS.signup.consumption.previousProductList);

    // TODO: Add case when we select not first option
    await page.waitForSelector(SELECTORS.signup.consumption.previousProductFirstOption);
    await page.focus(SELECTORS.signup.consumption.previousProductFirstOption);
    const prevProductOption = await page.$(SELECTORS.signup.consumption.previousProductFirstOption);
    await prevProductOption.evaluate(option => option.click());
}

// PaymentStep
export async function fillPaymentStep(page /* , options */) {
    await page.waitForSelector(SELECTORS.signup.payment.container);

    // TODO: Add cases with cryptocurrency + binanceToken, bitcoin
    // TODO: Add cases with paymentMethod = debit and iban + alternativeAccountHolder + sepaApproval

    await page.focus(SELECTORS.signup.payment.transferOption);
    const paymentTransferOption = await page.$(SELECTORS.signup.payment.transferOption);
    await paymentTransferOption.evaluate(option => option.click());
}

// BonusProgramStep
export async function fillBonusProgramStep(page, options) {
    await page.waitForSelector(SELECTORS.signup.bonusProgram.container);

    await page.focus(SELECTORS.signup.bonusProgram.bonusProgramRadioBtn);
    await page.click(SELECTORS.signup.bonusProgram.bonusProgramRadioBtn);

    if (options.withRightOfWithdrawal) {
        await page.focus(SELECTORS.signup.bonusProgram.bonusProgramRightOfWithdrawalCheckbox);
        await page.click(SELECTORS.signup.bonusProgram.bonusProgramRightOfWithdrawalCheckbox);
    }
}

// ConfirmationStep
export async function fillConfirmationStep(page, options = {}) {
    const { confirmationStep = { enableNotifications: false, withReferralCode: false, withSalesPartnerId: false } } =
        options;
    await page.waitForSelector(SELECTORS.signup.confirmation.container);

    // TODO: Add case with click to "Edit Step Button"
    // TODO: Add case with click to "Back icon"

    if (confirmationStep.withReferralCode) {
        await page.focus(SELECTORS.signup.confirmation.referralCodeInput);
        await page.type(SELECTORS.signup.confirmation.referralCodeInput, VALUES.referralCode);
    }

    if (confirmationStep.withSalesPartnerId) {
        await page.focus(SELECTORS.signup.confirmation.salesPartnerIdInput);
        await page.type(SELECTORS.signup.confirmation.salesPartnerIdInput, VALUES.salesPartnerId);
    }

    await page.focus(SELECTORS.signup.confirmation.agbApprovalCheckbox);
    const agbApprovalCheckbox = await page.$(SELECTORS.signup.confirmation.agbApprovalCheckbox);
    await agbApprovalCheckbox.evaluate(checkbox => checkbox.click());

    if (confirmationStep.enableNotifications) {
        await page.focus(SELECTORS.signup.confirmation.enableNotificationsCheckbox);
        const enableNotificationsCheckbox = await page.$(SELECTORS.signup.confirmation.enableNotificationsCheckbox);
        await enableNotificationsCheckbox.evaluate(checkbox => checkbox.click());
    }
}

// Common
export async function submitStepForm(page /* , options */) {
    await page.waitForTimeout(2000);
    await page.waitForSelector(SELECTORS.signup.nextButton);
    await page.focus(SELECTORS.signup.nextButton);
    await page.waitForTimeout(2000);
    await page.keyboard.type('\n'); // https://github.com/puppeteer/puppeteer/issues/1805#issuecomment-418965009
}

// Interception
const SIGN_UP_API = 'contract/create';

export async function interceptRequestSubmitSignUpData(page /* , options */) {
    page.on('request', async request => {
        if (request.url().includes(SIGN_UP_API) && request.method() === HTTP_METHODS.POST) {
            // TODO: Check the request.postData() values

            const updatedData = JSON.parse(request.postData());
            updatedData.googleReCaptchaResponse = GOOGLE_RECAPTCHA;

            await request.continue({
                postData: JSON.stringify(updatedData)
            });
        } else {
            await request.continue();
        }
    });
}

export async function interceptResponseSubmitSignUpData(page /* , options */) {
    page.on('response', async response => {
        if (response.url().includes(SIGN_UP_API) && response.request().method() === HTTP_METHODS.POST) {
            // TODO: Check the response.json() values
            // {
            //     "id": 1492,
            //     "orderId": 2776
            // }

            // todo: log error message, because it won't be visible on screenshot
            // https://jsoverson.medium.com/using-chrome-devtools-protocol-with-puppeteer-737a1300bac0
            // https://coderoad.ru/55408302/Как-получить-поток-буфер-с-помощью-кукольника#55422334
            const requestId = response.headers()['x-request-id'];
            console.log({ requestId });
            expect(response.status()).toBe(200);
        }
    });
}
