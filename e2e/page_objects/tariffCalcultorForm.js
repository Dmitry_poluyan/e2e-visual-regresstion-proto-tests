import config from '../e2e.config';

const { SELECTORS, VALUES } = config;

export async function handleTariffCalculatorForm(page /* , options */) {
    // TODO: add gas input ability for v1 and v2 (@Anton)
    // TODO: add options with other test cases strategy (@Anton)
    const tariffCalculatorFormV2 = await page.$(SELECTORS.tariffCalculatorForm.tariffCalculatorFormV2);

    if (tariffCalculatorFormV2) {
        await page.click(SELECTORS.tariffCalculatorForm.tariffCalculatorFormV2); // For stop TopIndexSection timer
        return void (await inputTariffCalculatorFormV2(page));
    }

    console.warn('Did not find any calculator form');
}

export async function submitTariffCalculatorForm(page /* , options */) {
    const tariffCalculatorFormV2 = await page.$(SELECTORS.tariffCalculatorForm.tariffCalculatorFormV2);

    if (tariffCalculatorFormV2) {
        await page.waitForSelector(SELECTORS.tariffCalculatorForm.tariffCalculateButtonV2);
        await page.focus(SELECTORS.tariffCalculatorForm.tariffCalculateButtonV2);
        return void (await page.click(SELECTORS.tariffCalculatorForm.tariffCalculateButtonV2));
    }

    console.warn('Did not find any calculator submit button form');
}

async function inputTariffCalculatorFormV2(page /* , options */) {
    // TODO: add gas input ability with diff zip and consumption (@Anton)
    // TODO: add options with other test cases strategy (@Anton)

    // TODO: Clear input fields befire type new data (@Anton)
    // await page.evaluate(() => (page.$(SELECTORS.tariffCalculatorForm.addressInput, VALUES.address).value = ''));

    const isZipMode = await page.$(SELECTORS.tariffCalculatorForm.zipInput);

    if (isZipMode) {
        await page.waitForSelector(SELECTORS.tariffCalculatorForm.zipInput);
        await page.focus(SELECTORS.tariffCalculatorForm.zipInput);
        await page.type(SELECTORS.tariffCalculatorForm.zipInput, VALUES.zip);
    } else {
        await page.waitForSelector(SELECTORS.tariffCalculatorForm.addressInput);
        await page.focus(SELECTORS.tariffCalculatorForm.addressInput);
        await page.type(SELECTORS.tariffCalculatorForm.addressInput, VALUES.address);

        await page.waitForSelector(SELECTORS.autocomplete.container);
        await page.waitForSelector(SELECTORS.autocomplete.firstItem);
        await page.focus(SELECTORS.autocomplete.firstItem);
        await page.click(SELECTORS.autocomplete.firstItem); // FIXME: Sometimes it does not found slowMo: 30 helps with this. Need find other solution (@Pavel)
    }

    await page.waitForSelector(SELECTORS.tariffCalculatorForm.electricityConsumptionInputV2);
    await page.focus(SELECTORS.tariffCalculatorForm.electricityConsumptionInputV2);
    await page.type(SELECTORS.tariffCalculatorForm.electricityConsumptionInputV2, VALUES.consumption);

    await submitTariffCalculatorForm(page);
}
