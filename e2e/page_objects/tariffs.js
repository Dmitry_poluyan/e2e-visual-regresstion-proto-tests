import config from '../e2e.config';

const { SELECTORS } = config;

export async function chooseTariff(page, options = {}) {
    const tariffCardSelector = options.tariffChooseButton || SELECTORS.tariffs.tariffChooseButton;

    await page.waitForSelector(SELECTORS.tariffs.tariffList);
    await page.waitForSelector(tariffCardSelector);
    await page.focus(tariffCardSelector);

    const signUpButton = await page.$(tariffCardSelector);
    await signUpButton.evaluate(button => button.click());
}

export async function checkTariffCards(page /* , options */) {
    // TODO: Check count of tariff cards (@Anton)
    // TODO: Check tariff cards prices (@Anton)
    // TODO: Check pp cards when it with tariff (@Anton)

    await page.waitForSelector(SELECTORS.tariffs.tariffList);
    await page.waitForSelector(SELECTORS.tariffs.tariffCard);
    await page.focus(SELECTORS.tariffs.tariffCard);

    const cards = await page.$$(SELECTORS.tariffs.tariffCard);

    expect(cards.length > 0).toBe(true);
}
