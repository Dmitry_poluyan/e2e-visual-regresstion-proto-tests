import { TIMEOUT, USER_AGENT } from '../helpers/commonConfig.js';
// TODO need to config import/export correctly. Do not remove index.js from import below!
import { getprotoHost, getTPHost, isDevEnv, isProdEnv } from '../helpers/index.js';

// TODO: Fix eslint (R)
// TODO: Fix .babelrc (R)

// TODO: Add test cases with:
//  - HT/NT tariffs and sign up logic @Anton
//  - with diff variants of protos and tariffs pages https://proto.atlassian.net/browse/SW-1014 @Dzmitry
//  - with powerPlant tariff @Dzmitry
//  - cover TP with e2e (R)

export const proto_DE_HOST = getprotoHost();
export const TP_HOST = getTPHost();

const e2eConfig = {
    proto_DE_HOST,
    TP_HOST,

    SCREENSHOTS_PATH: (prefix = '') => `./screenshots/${prefix}${Date.now()}.png`,

    TEST_PAGES: {
        protosLandingPages: [
            { title: 'Address based, V2 calculator', url: proto_DE_HOST, options: { skipThisTestCase: false } },
            {
                title: 'Zip based, V2 calculator',
                url: `${proto_DE_HOST}/proto-strom`,
                options: { skipThisTestCase: false }
            }
        ],
        protosPages: [
            {
                title: 'Zip based, V2 calculator',
                url: `${proto_DE_HOST}/protos?zip=10115&consumption=1600&energyType=electricity&identifier=proto-strom&ascDesc=ASC&orderBy=price&isEcoenergy=true`,
                options: { skipThisTestCase: false }
            },
            {
                title: 'Address based, V2 calculator',
                url: `${proto_DE_HOST}/protos?zip=13159&consumption=1600&houseNumber=1&streetName=protoser%20Mauerweg&cityName=protos&energyType=electricity&identifier=standard&ascDesc=ASC&orderBy=price`,
                options: { skipThisTestCase: false }
            },
            {
                title: 'With default params',
                url: `${proto_DE_HOST}/protos`,
                options: { skipThisTestCase: false }
            }
        ],
        producerDetailsPages: [
            {
                title: 'Zip based, V1 calculator',
                url: `${proto_DE_HOST}/producer-details/1-proto-oekostrommix?zip=10115&consumption=1600&energyType=electricity&identifier=proto-strom&ascDesc=ASC&orderBy=price&isEcoenergy=true`,
                options: { skipThisTestCase: false }
            },
            {
                title: 'Address based, V2 calculator',
                url: `${proto_DE_HOST}/producer-details/32-gronover-energy-one?zip=13159&consumption=1600&houseNumber=1&streetName=protoser%20Mauerweg&cityName=protos&energyType=electricity&identifier=standard&ascDesc=ASC&orderBy=price`,
                options: { skipThisTestCase: false }
            },
            {
                title: 'With default params',
                url: `${proto_DE_HOST}/producer-details/32-gronover-energy-one`,
                options: { skipThisTestCase: false }
            }
        ],
        tariffsLandingPages: [
            {
                title: 'Address based, V2 calculator, partner',
                url: `${proto_DE_HOST}/proto`,
                options: { skipThisTestCase: false }
            },
            {
                title: 'Zip based, V2 calculator',
                url: `${proto_DE_HOST}/partners/proto`,
                options: {
                    isPowerPlantTariffs: true,
                    skipThisTestCase: false
                }
            }
        ],
        tariffsPages: [
            {
                title: 'Address based, V2 calculator, partner',
                url: `${proto_DE_HOST}/contract/proto?zip=13159&consumption=1600&houseNumber=1&streetName=protoser%20Mauerweg&cityName=protos&energyType=electricity`,
                options: { skipThisTestCase: false }
            },
            {
                title: 'Address based, V2 calculator, partner, gas',
                url: `${proto_DE_HOST}/partners/contract/es24?zip=13159&consumption=1600&houseNumber=1&streetName=protoser%20Mauerweg&cityName=protos&energyType=gas`,
                options: { skipThisTestCase: isProdEnv() }
            },
            {
                title: 'Zip based, V2 calculator, partner, PowerPlantTariffs',
                url: `${proto_DE_HOST}/contract/proto?zip=10115&consumption=1600&energyType=electricity`,
                options: {
                    isPowerPlantTariffs: true,
                    skipThisTestCase: false
                }
            },
            {
                title: 'With default params',
                url: `${proto_DE_HOST}/contract/proto`,
                options: { skipThisTestCase: false }
            }
        ],
        signUpPages: [
            {
                title: 'Address based, V2 calculator, partner, with bonus step, rightOfWithdrawal',
                url: `${proto_DE_HOST}/partners/signup/proto?zip=13355&consumption=1600&houseNumber=111&streetName=Bernauer%20Str.&cityName=protos&energyType=electricity&producerId=1&signupFlow=proto&tariffId=101`,
                options: {
                    identityStep: { salutation: 0 },
                    checkDb: false,
                    checkEmail: false,
                    bonusProgramStep: { withRightOfWithdrawal: true }, // TODO: Need check db result https://proto.atlassian.net/browse/SW-1380 (@Eugene)
                    checkLoginToTP: false,
                    skipThisTestCase: true // TODO: Now we don't have business logic with bonus steps. Probably we need remove all related code in future.
                }
            },
            {
                title: 'Zip based, V2 calculator, without address in url, withNewPassword',
                url: `${proto_DE_HOST}/signup/standard?zip=10115&consumption=1600&energyType=electricity&producerId=1&signupFlow=standard`,
                options: {
                    checkDb: false,
                    checkEmail: false,
                    identityStep: {
                        salutation: 4,
                        useRandomFirstName: true,
                        useRandomLastName: true
                    },
                    contactStep: { withNewPassword: true },
                    checkLoginToTP: true, // Should work only with withNewPassword: true or withExistedEmail: true, because only in this cases we know the password
                    skipThisTestCase: false
                }
            },
            {
                title: 'Address based, V2 calculator, partner, business, defaultAddressFromURL, enableNotifications, withReferralCode',
                url: `${proto_DE_HOST}/signup/proto?zip=13159&consumption=1600&houseNumber=1&streetName=protoser%20Mauerweg&cityName=protos&energyType=electricity&producerId=1&signupFlow=proto&tariffId=18`,
                options: {
                    identityStep: { business: true },
                    addressStep: { useDefaultAddressFromURL: false },
                    confirmationStep: { enableNotifications: true, withReferralCode: true },
                    expectedValues: { salesPersonId: 4 },
                    checkDb: false, // TODO: Fix this in the https://proto.atlassian.net/browse/SW-1380 (@Eugene)
                    checkEmail: false, // TODO: Fix this in the https://proto.atlassian.net/browse/SW-1380 (@Eugene)
                    skipThisTestCase: false
                }
            },
            {
                title: 'Zip based, V2 calculator, partner, without address in url, withExistedEmail, without reCaptcha',
                url: `${proto_DE_HOST}/signup/proto?zip=10115&consumption=1600&energyType=electricity&producerId=1&signupFlow=proto&tariffId=206`,
                options: {
                    checkDb: false, // TODO: Fix this in the https://proto.atlassian.net/browse/SW-1380 (@Eugene)
                    checkEmail: false,
                    identityStep: { salutation: 2 },
                    contactStep: { withExistedEmail: false }, // TODO: set true after https://proto.atlassian.net/browse/SW-1434 and 1556 (@Eugene)
                    skipThisTestCase: isProdEnv()
                }
            },
            {
                title: 'Address based, V2 calculator, partner, used with reverse proxy',
                url: `${proto_DE_HOST}/signup/proto?zip=60385&consumption=1600&houseNumber=162&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&producerId=1&signupFlow=Sparwelt&tariffId=147`,
                options: {
                    checkDb: false,
                    checkEmail: false,
                    identityStep: { salutation: 3 },
                    contactStep: { withExistedEmail: false },
                    skipThisTestCase: false
                }
            },
            {
                title: 'Address based, V2 calculator, partner, gas, defaultAddressFromURL, withSalesPartnerId',
                url: `${proto_DE_HOST}/signup/hw?zip=60385&consumption=1600&houseNumber=121&streetName=Berger%20Str.&cityName=Frankfurt%20am%20Main&energyType=electricity&producerId=1&signupFlow=hw&tariffId=103`,
                options: {
                    identityStep: { salutation: 4 },
                    addressStep: { useDefaultAddressFromURL: false },
                    confirmationStep: { withSalesPartnerId: true },
                    checkDb: false,
                    checkEmail: false,
                    skipThisTestCase: false
                }
            },
            {
                title: 'Zip based, V2 calculator, defaultAddressFromURL, business, default birthday',
                url: `${proto_DE_HOST}/signup/standard?zip=10719&consumption=1600&houseNumber=21&streetName=protos&cityName=protos&energyType=electricity&producerId=32&signupFlow=standard`,
                options: {
                    identityStep: { business: true, withDefaultBirthday: true },
                    addressStep: { useDefaultAddressFromURL: false },
                    checkDb: false,
                    checkEmail: false, // FIXME: Doesn't work for standard sign up (@Eugene)
                    skipThisTestCase: false
                }
            },
            {
                title: 'Zip based, V2 calculator, cost plus (with gridId)',
                url: `${proto_DE_HOST}/signup/team-proto?energyType=electricity&producerId=1&consumption=2400&zip=82380&signupFlow=team-proto&tariffId=359&productId=costplus&variant=24months&subvariant=recurring&ewp=0&ebp=0&gridId=9901068000001`,
                options: {
                    identityStep: { business: false },
                    addressStep: { useDefaultAddressFromURL: false },
                    confirmationStep: { withSalesPartnerId: true },
                    checkDb: false,
                    checkEmail: false,
                    skipThisTestCase: false
                }
            },
            {
                title: 'Gas, Zip based, V2 calculator, cost plus (with gridId)',
                url: `${proto_DE_HOST}/signup/team-proto?energyType=gas&producerId=50&consumption=2400&zip=02943&signupFlow=team-proto&tariffId=360&productId=costplus&variant=24months&subvariant=recurring&ewp=0&ebp=0&gridId=9870008200006`,
                options: {
                    identityStep: { business: false },
                    addressStep: { useDefaultAddressFromURL: false },
                    confirmationStep: { withSalesPartnerId: true },
                    checkDb: false,
                    checkEmail: false,
                    skipThisTestCase: isProdEnv()
                }
            },
            {
                title: 'With default params',
                url: `${proto_DE_HOST}/signup/standard?producerId=1`,
                options: {
                    checkDb: false,
                    checkEmail: false,
                    identityStep: { salutation: 3 },
                    contactStep: { withExistedEmail: false },
                    skipThisTestCase: false
                }
            }
        ]
    },

    USER_AGENT,

    LAUNCH_OPTIONS: {
        headless: !isDevEnv(),
        defaultViewport: {
            width: 1920,
            height: 1200,
            isLandscape: true
        },
        devtools: false,
        args: ['--no-sandbox'],
        slowMo: isDevEnv() ? 90 : 80,
        timeout: TIMEOUT
    },

    NEW_PAGE_CONFIG: {
        timeout: TIMEOUT
    },

    GO_TO_CONFIG: {
        waitUntil: isDevEnv() ? ['load', 'domcontentloaded'] : ['load', 'networkidle0', 'domcontentloaded'],
        timeout: TIMEOUT
    },

    VALUES: {
        // Tariff form
        address: 'protos 21a, protos, protos',
        consumption: '1600',
        gasConsumption: '7500',
        zip: '10719',

        // Sign up - idenitity step
        firstName: 'test',
        lastName: 'test',
        birthday: '28.04.1994',
        company: 'e2e-test-company',
        legalForm: 'e2e-legal-form',

        // Sign up - contact step
        existedEmail: isProdEnv() ? 'protos@proto.de' : 'e2e-test@proto-proto.de',
        existedPassword: isProdEnv() ? 'Test12345' : 'testTest1',
        password: 'testTest1',
        phoneAreaCode: '37529',
        phone: '5041301',

        // Sign up - address
        street: 'protos',
        streetNumber: '22a',

        // Sign up - confirmation
        referralCode: 'OKPOWER',
        salesPartnerId: '4711'
    },
    SELECTORS: {
        autocomplete: {
            container: '.pac-container',
            firstItem: '.pac-container .pac-item'
        },
        tariffCalculatorForm: {
            tariffCalculatorFormV2: '.proto-de-partners-landing-tariff-calculator--v2 form',
            zipInput: 'input[name="zip"]',
            addressInput: '.proto-de-site-location-input > input',
            consumptionInput: 'input[name="consumption"]',
            electricityConsumptionInputV2: 'input[name="electricityConsumption"]',
            gasConsumptionInputV2: 'input[name="gasConsumption"]',
            tariffCalculateButtonV2: '.proto-de-tariff-calculator-form-submit'
        },
        landing: {
            container: '.proto-de-partners-landing'
        },
        protos: {
            container: '.proto-de-protos-page',
            producerList: '.proto-de-protos-section',
            producerCard: '.proto-de-protos-section .proto-producer-data',
            producerChooseButton: '.proto-producer-data-offer-button',
            producerDetailsButton: '.proto-producer-data-detail-button',
            desktopTariffCalcContainer: '.proto-de-protos-page-controls'
        },
        producerDetails: {
            container: '.proto-de-producer-details-layout',
            becomeCustomerButton: '.proto-producer-details-card-confirmation a'
        },
        tariffs: {
            container: '.proto-de-partners-contract',
            tariffList: '.proto-de-partners-contract-tariff-card',
            tariffCard: '.proto-de-partners-contract-tariff-card-content',
            tariffChooseButton: '.proto-de-partners-contract-tariff-card-actions a'
        },
        contract: {
            container: '.proto-de-partners-contract'
        },
        signup: {
            container: '.proto-de-partners-sign-up',
            registrationForm: '.proto-registration-form-steps',
            nextButton: '.proto-wizard-content-actions > a[role="button"]',
            identity: {
                container: '.proto-identity-form',
                company: 'input[name="company"]',
                legalForm: 'input[name="legalForm"]',
                firstName: 'input[name="firstName"]',
                lastName: 'input[name="lastName"]',
                salutation: 'input[name="salutation"]',
                business: '.proto-identity-form .proto-radio-button:last-child',
                salutationList: '#listbox-salutation',
                salutationOption: {
                    0: '.proto-select-option:nth-child(1)',
                    1: '.proto-select-option:nth-child(2)',
                    2: '.proto-select-option:nth-child(3)',
                    3: '.proto-select-option:nth-child(4)',
                    4: '.proto-select-option:nth-child(5)'
                },
                birthday: 'input[name="birthday"]'
            },
            contact: {
                container: '#contactForm',
                email: 'input[name="email"]',
                password: 'input[name="password"]',
                passwordConfirmation: 'input[name="passwordConfirmation"]',
                phoneAreaCode: 'input[name="phoneAreaCode"]',
                phone: 'input[name="phone"]',
                oldAccountCheckbox: '#contactForm .proto-radio-button:first-child'
            },
            address: {
                container: '#addressForm',
                street: 'input[name="street"]',
                streetNumber: 'input[name="streetNumber"]',
                zip: 'input[name="postcode"]'
            },
            consumption: {
                container: '#consumptionForm',
                counterNumber: 'input[name="counterNumber"]',
                counterNumberCheckbox: '.proto-registration-form-field:first-child .proto-radio-button:last-child', // 'input[value="counterNumber"]'
                marketLocationIdentifier: 'input[name="marketLocationIdentifier"]',
                terminationDate: 'input[name="terminationDate"]',
                previousProduct: '#previous-product input[name="previousProduct"]',
                previousProductList: '#listbox-previous-product',
                previousProductFirstOption: '#listbox-previous-product .proto-select-option'
            },
            payment: {
                container: '#paymentInfoForm',
                cryptocurrency: 'input[value="cryptocurrency"]',
                binanceToken: 'input[value="binanceToken"]',
                transferOption: 'input[value="transfer"]'
            },
            bonusProgram: {
                container: '#bonusProgramForm',
                bonusProgramRadioBtn: '.proto-registration-form-bonus-card-body .proto-radio-button:last-child',
                bonusProgramRightOfWithdrawalCheckbox: '.proto-checkbox .proto-checkbox-wrapper'
            },
            confirmation: {
                container: '#confirmationForm',
                agbApprovalCheckbox: 'input[name="agbApproval"]',
                enableNotificationsCheckbox: 'input[name="enableNotifications"]',
                referralCodeInput: 'input[name="referralCode"]',
                salesPartnerIdInput: 'input[name="salesPartnerId"]'
            }
        },
        thankyou: {
            container: '.proto-de-thank-you',
            content: '.proto-de-thank-you-content',
            portalButton: '.proto-button'
        },
        tradingPortal: {
            loginPage: {
                container: '.login-container',
                usernameInput: 'input[name="username"]',
                passwordInput: 'input[name="password"]',
                submitButton: '.login-form .proto-button'
            },
            dashboardPage: {
                container: '#app-layout'
            }
        }
    },

    HTTP_METHODS: {
        POST: 'POST'
    },

    GOOGLE_RECAPTCHA: '',

    E2E_STATUSES: {
        START: 'start',
        SUCCESS: 'success',
        FAIL: 'fail'
    }
};

export default e2eConfig;
