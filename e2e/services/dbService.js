import dotenv from 'dotenv';
import knex from 'knex';
import config from '../e2e.config';
import { delay } from '../../helpers';

dotenv.config();

let db;

function init() {
    db = knex({
        client: process.env.DATABASE_ENGINE,
        connection: {
            host: process.env.DATABASE_HOST,
            user: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME
        }
    });
}

function tear() {
    return db.destroy();
}

export async function checkDb(options) {
    init();

    const { testValues = {}, expectedValues = {}, contactStep: { withExistedEmail = false } = {} } = options;

    const email = withExistedEmail ? config.VALUES.existedEmail : testValues.email;
    const account = await db('account').where({ email }).first();

    // todo: powercloud match with existing account so we have a lot of customers with the same powercloudCustomerId (@Eugene)
    // try to use slightly different input data for each test to make following part more reliable
    const customer = await db('customer')
        .orderBy('id', 'desc')
        .where({ accountId: account.id })
        .orderBy('createdAt', 'desc')
        .first();

    await delay(); // powercloudContractId is set with delay

    const contract = await db('contract').where({ customerId: customer.id }).first();

    expect(contract.powercloudOrderId).toBeGreaterThan(0);

    // TODO: We need investigate and enable the check for sign up with existed email (@Eugene)
    if (contract.salesFunnel !== 'standard' && !withExistedEmail) {
        expect(contract.powercloudContractId).toBeGreaterThan(0);

        const contractPc = await db('contract_pc').where({ contractId: contract.powercloudContractId }).first();
        expect(contractPc.data).toMatchObject({
            firstname: config.VALUES.firstName,
            surname: config.VALUES.lastName,
            counterNumber: testValues.counterNumber
        });

        const ordersPc = await db('orders_pc').where({ contractId: contract.powercloudContractId }).first();
        expect(ordersPc.data).toMatchObject({
            street: config.VALUES.street
        });

        if (expectedValues.salesPersonId) {
            const contractCommission = await db('contract_commission').where({ contractId: contract.id }).first();
            expect(contractCommission.salesPersonId).toEqual(expectedValues.salesPersonId);
        }
    }

    tear();
}
