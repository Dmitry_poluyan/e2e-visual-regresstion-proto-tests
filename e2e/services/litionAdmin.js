import Axios from 'axios';
import config from '../e2e.config';
import { APP_ENV } from '../../helpers/commonConfig';

const proto_ADMIN_URL = 'https://admin.proto.de';

const proto_ADMIN_ENV = {
    PRODUCTION: 'prod',
    STAGING: 'staging'
};

export async function logTestEvent(testCaseName, status = config.E2E_STATUSES.START, env = process.env.APP_ENV) {
    const protoAdminEnv = env === APP_ENV.PRODUCTION ? proto_ADMIN_ENV.PRODUCTION : proto_ADMIN_ENV.STAGING;
    const url = `${proto_ADMIN_URL}/tools/logTestEvent.php?job="${encodeURIComponent(
        testCaseName
    )}"&envOfEvent=${protoAdminEnv}&status=${status}`;

    console.info('logTestEvent', url);
    return Axios.get(url)
        .then(result => result?.data)
        .catch(error => console.error(error?.response?.data));
}
