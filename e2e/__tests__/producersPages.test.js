import config from '../e2e.config';
import { checkPowerPlantCards, choosePowerPlant, goToProducerDetails } from '../page_objects/protos';
import { e2eIt } from '../page_objects/hof';

const {
    SELECTORS,
    GO_TO_CONFIG,
    TEST_PAGES: { protosPages }
} = config;

describe('Producer pages.', () => {
    // TODO: add gas test cases (@Anton)
    // TODO: add both energy types test cases (@Anton)
    // TODO: add V2 producer page test cases (@Anton)
    // TODO: add case when clean the fields of the form and filling again, then submit (@Anton)

    describe('To sign up.', () => {
        for (let i = 0; i < protosPages.length; i++) {
            const { url: URL, options = {}, title = URL } = protosPages[i];

            e2eIt(title, options, async (browser, page, done) => {
                console.info('URL', URL);

                await page.goto(URL, GO_TO_CONFIG);

                await page.waitForSelector(SELECTORS.protos.container);
                await checkPowerPlantCards(page, options);

                // TODO: Submit the form again for check if it works (@Anton)
                // There on the page a few calculators (popup, accordion, desktop)
                // await submitTariffCalculatorForm(page, options);
                // await page.waitForSelector(SELECTORS.protos.container);
                // await checkPowerPlantCards(page, options);

                await choosePowerPlant(page, options);

                await page.waitForSelector(SELECTORS.signup.container);

                // TODO: Check sign up card data (prices) (@Anton)

                done();
            });
        }
    });

    describe('To details.', () => {
        for (let i = 0; i < protosPages.length; i++) {
            const { url: URL, options = {}, title = URL } = protosPages[i];

            e2eIt(title, options, async (browser, page, done) => {
                console.info('URL', URL);

                await page.goto(URL, GO_TO_CONFIG);

                await page.waitForSelector(SELECTORS.protos.container);
                await checkPowerPlantCards(page, options);

                await goToProducerDetails(page, options);

                await page.waitForSelector(SELECTORS.producerDetails.container);

                // TODO: Check data (prices) (@Anton)

                done();
            });
        }
    });
});
