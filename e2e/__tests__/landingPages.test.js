import config from '../e2e.config';
import { handleTariffCalculatorForm } from '../page_objects/tariffCalcultorForm';
import { checkPowerPlantCards } from '../page_objects/protos';
import { checkTariffCards } from '../page_objects/tariffs';
import { e2eIt } from '../page_objects/hof';

const {
    SELECTORS,
    GO_TO_CONFIG,
    TEST_PAGES: { protosLandingPages, tariffsLandingPages }
} = config;

describe('Landing pages.', () => {
    describe('To protos page.', () => {
        for (let i = 0; i < protosLandingPages.length; i++) {
            const { url: URL, options = {}, title = URL } = protosLandingPages[i];

            e2eIt(title, options, async (browser, page, done) => {
                console.info('URL', URL);

                await page.goto(URL, GO_TO_CONFIG);

                await page.waitForSelector(SELECTORS.landing.container);

                await handleTariffCalculatorForm(page, options);

                await page.waitForSelector(SELECTORS.protos.container);
                await checkPowerPlantCards(page, options);

                done();
            });
        }
    });

    describe('To contract page.', () => {
        for (let i = 0; i < tariffsLandingPages.length; i++) {
            const { url: URL, options = {}, title = URL } = tariffsLandingPages[i];

            e2eIt(title, options, async (browser, page, done) => {
                console.info('URL', URL);

                await page.goto(URL, GO_TO_CONFIG);

                await page.waitForSelector(SELECTORS.landing.container);

                await handleTariffCalculatorForm(page, options);

                await page.waitForSelector(SELECTORS.contract.container);

                if (options.isPowerPlantTariffs) {
                    await checkPowerPlantCards(page, options);
                } else {
                    await checkTariffCards(page, options);
                }

                done();
            });
        }
    });
});
