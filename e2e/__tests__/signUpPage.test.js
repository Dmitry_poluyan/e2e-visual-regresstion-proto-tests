import config from '../e2e.config';
import { isProdEnv, isStagingEnv } from '../../helpers';
import {
    fillAddressStep,
    fillBonusProgramStep,
    fillConfirmationStep,
    fillConsumptionStep,
    fillContactStep,
    fillIdentityStep,
    fillPaymentStep,
    interceptRequestSubmitSignUpData,
    interceptResponseSubmitSignUpData,
    submitStepForm
} from '../page_objects/signUp';
import { checkDb } from '../services/dbService';
import { loginToTradingPortal } from '../page_objects/traidingPortal';
import { generateEmailAddress, getLastMessage } from '../page_objects/services/emailService';
import { e2eIt } from '../page_objects/hof';

import { acceptCookies } from '../../visual_regression/testHelpers';
import { generateCounterNumber, generateEmail } from '../../helpers/generators';

const {
    SELECTORS,
    GO_TO_CONFIG,
    TEST_PAGES: { signUpPages }
} = config;

describe('Sign up pages.', () => {
    describe('Registration.', () => {
        for (let i = 0; i < signUpPages.length; i++) {
            const { url: URL, options = {}, title = URL } = signUpPages[i];

            e2eIt(title, options, async (browser, page, done) => {
                console.info('URL', URL);

                const counterNumber = generateCounterNumber();
                let email = generateEmail();
                if (options.checkEmail && isStagingEnv()) {
                    email = await generateEmailAddress(browser);
                    console.log({ email });
                }
                const testOptions = { ...options, testValues: { email, counterNumber } };
                await page.bringToFront();

                await page.goto(URL, GO_TO_CONFIG);
                await page.setRequestInterception(true);
                await interceptRequestSubmitSignUpData(page, testOptions);
                await interceptResponseSubmitSignUpData(page, testOptions);

                // TODO: Add for every test page (@Pavel)
                await page.evaluate(acceptCookies);

                // TODO: Check price card (@Anton)

                await page.waitForSelector(SELECTORS.signup.container);
                await page.waitForSelector(SELECTORS.signup.registrationForm);

                await fillIdentityStep(page, testOptions);
                await submitStepForm(page, testOptions);

                await fillContactStep(page, testOptions);
                await submitStepForm(page, testOptions);

                await fillAddressStep(page, testOptions);
                await submitStepForm(page, testOptions);

                await fillConsumptionStep(page, testOptions);
                await submitStepForm(page, testOptions);

                await fillPaymentStep(page, testOptions);
                await submitStepForm(page, testOptions);

                if (options.bonusProgramStep) {
                    await fillBonusProgramStep(page, testOptions);
                    await submitStepForm(page, testOptions);
                }

                await fillConfirmationStep(page, testOptions);
                // TODO: Check confirmation page data (@Anton)

                console.info('Should stop execution here for production!!!');

                if (!isProdEnv()) {
                    console.info('IMPORTANT: This code is forbidden to run in production!');

                    await submitStepForm(page, testOptions);

                    await page.waitForSelector(SELECTORS.thankyou.container);

                    if (testOptions.checkLoginToTP) {
                        await loginToTradingPortal(page, testOptions);
                        await page.waitForSelector(SELECTORS.tradingPortal.dashboardPage.container);
                    }

                    if (testOptions.checkDb) {
                        await checkDb(testOptions);
                    }

                    // may take up to 5 min
                    if (testOptions.checkEmail) {
                        const message = await getLastMessage();
                        expect(message).toMatch(new RegExp('High Fives!'));
                    }
                }

                done();
            });
        }
    });
});
