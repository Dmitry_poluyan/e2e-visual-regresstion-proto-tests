import config from '../e2e.config';
import { clickBecomeCustomerButton } from '../page_objects/protos';
import { e2eIt } from '../page_objects/hof';

const {
    SELECTORS,
    GO_TO_CONFIG,
    TEST_PAGES: { producerDetailsPages }
} = config;

describe('Producer details page.', () => {
    // TODO: add gas test cases (@Anton)
    // TODO: add V2 producer page test cases (@Anton)
    // TODO: add case when clean the fields of the form and filling again, then submit (@Anton)

    describe('To sign up page.', () => {
        for (let i = 0; i < producerDetailsPages.length; i++) {
            const { url: URL, options = {}, title = URL } = producerDetailsPages[i];

            e2eIt(title, options, async (browser, page, done) => {
                console.info('URL', URL);

                await page.goto(URL, GO_TO_CONFIG);

                await page.waitForSelector(SELECTORS.producerDetails.container);

                await clickBecomeCustomerButton(page, options);

                await page.waitForSelector(SELECTORS.signup.container);

                // TODO: Check data (prices), info (@Anton)

                done();
            });
        }
    });
});
