import config from '../e2e.config';
import { checkTariffCards, chooseTariff } from '../page_objects/tariffs';
import { checkPowerPlantCards, choosePowerPlant } from '../page_objects/protos';
import { e2eIt } from '../page_objects/hof';

const {
    SELECTORS,
    GO_TO_CONFIG,
    TEST_PAGES: { tariffsPages }
} = config;

describe('Tariff pages', () => {
    // TODO: add gas test cases (@Anton)
    // TODO: add both energy types test cases (@Anton)

    describe('To sign up.', () => {
        for (let i = 0; i < tariffsPages.length; i++) {
            const { url: URL, options = {}, title = URL } = tariffsPages[i];

            e2eIt(title, options, async (browser, page, done) => {
                console.info('URL', URL);

                await page.goto(URL, GO_TO_CONFIG);

                await page.waitForSelector(SELECTORS.tariffs.container);

                if (options.isPowerPlantTariffs) {
                    await checkPowerPlantCards(page, options);
                    await choosePowerPlant(page, options);
                } else {
                    await checkTariffCards(page, options);

                    // TODO: Submit the form again for check if it works (@Anton)
                    // await handleTariffCalculatorForm(page);
                    // await page.waitForSelector(SELECTORS.protos.container);
                    // await checkTariffCards(page, options);
                    await chooseTariff(page, options);
                }

                await page.waitForSelector(SELECTORS.signup.container);

                // TODO: Check sign up card data (prices) (@Anton)
                done();
            });
        }
    });
});
